package com.example.demo_luyen_thi.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo_luyen_thi.model.Phongban;

public interface PhongbanRepository extends JpaRepository<Phongban, Long> {

}
