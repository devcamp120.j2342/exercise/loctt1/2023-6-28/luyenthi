package com.example.demo_luyen_thi.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo_luyen_thi.model.Nhanvien;

public interface NhanvienRepository extends JpaRepository<Nhanvien, Long> {

    List<Nhanvien> findByPhongbanId(Long userId);

}
