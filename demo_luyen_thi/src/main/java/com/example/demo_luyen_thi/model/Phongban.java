package com.example.demo_luyen_thi.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "Phòng_Ban")
public class Phongban {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "mã_phòng_ban")
    private String maPhongBan;
    @Column(name = "tên_phòng_ban")
    private String tenPhongBan;
    @Column(name = "nghiệp_vụ")
    private String nghiepVu;
    @Column(name = "giới_thiệu")
    private String gioiThieu;
    @OneToMany(mappedBy = "phongban", cascade = CascadeType.ALL)
    @JsonManagedReference
    private List<Nhanvien> nhanviens;

    public Phongban() {
        super();
    }

    public Phongban(long id, String maPhongBan, String tenPhongBan, String nghiepVu, String gioiThieu,
            List<Nhanvien> nhanviens) {
        super();
        this.maPhongBan = maPhongBan;
        this.tenPhongBan = tenPhongBan;
        this.nghiepVu = nghiepVu;
        this.gioiThieu = gioiThieu;
        this.nhanviens = nhanviens;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMaPhongBan() {
        return maPhongBan;
    }

    public void setMaPhongBan(String maPhongBan) {
        this.maPhongBan = maPhongBan;
    }

    public String getTenPhongBan() {
        return tenPhongBan;
    }

    public void setTenPhongBan(String tenPhongBan) {
        this.tenPhongBan = tenPhongBan;
    }

    public String getNghiepVu() {
        return nghiepVu;
    }

    public void setNghiepVu(String nghiepVu) {
        this.nghiepVu = nghiepVu;
    }

    public String getGioiThieu() {
        return gioiThieu;
    }

    public void setGioiThieu(String gioiThieu) {
        this.gioiThieu = gioiThieu;
    }

    public List<Nhanvien> getNhanviens() {
        return nhanviens;
    }

    public void setNhanviens(List<Nhanvien> nhanviens) {
        this.nhanviens = nhanviens;
    }

}
