package com.example.demo_luyen_thi.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "Nhân_viên")
public class Nhanvien {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(name = "mã_nhân_viên")
    private String maNhanVien;
    @Column(name = "tên_nhân_viên")
    private String tenNhanVien;
    @Column(name = "chức_vụ")
    private String chucVu;
    @Column(name = "giới_tính")
    private String gioiTinh;
    @Column(name = "ngày_sinh")
    private String ngaySinh;
    @Column(name = "dia_chi")
    private String diachi;
    @Column(name = "số_điện_thoại_liên_hệ")
    private String soDienThoaiLienHe;
    @ManyToOne
    @JoinColumn(name = "phongban_id", referencedColumnName = "id")
    @JsonBackReference
    private Phongban phongban;

    public Nhanvien() {
        super();
    }

    public Nhanvien(long id, String maNhanVien, String tenNhanVien, String chucVu, String gioiTinh, String ngaySinh,
            String diachi, String soDienThoaiLienHe, Phongban phongban) {
        super();
        this.maNhanVien = maNhanVien;
        this.tenNhanVien = tenNhanVien;
        this.chucVu = chucVu;
        this.gioiTinh = gioiTinh;
        this.ngaySinh = ngaySinh;
        this.diachi = diachi;
        this.soDienThoaiLienHe = soDienThoaiLienHe;
        this.phongban = phongban;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMaNhanVien() {
        return maNhanVien;
    }

    public void setMaNhanVien(String maNhanVien) {
        this.maNhanVien = maNhanVien;
    }

    public String getTenNhanVien() {
        return tenNhanVien;
    }

    public void setTenNhanVien(String tenNhanVien) {
        this.tenNhanVien = tenNhanVien;
    }

    public String getChucVu() {
        return chucVu;
    }

    public void setChucVu(String chucVu) {
        this.chucVu = chucVu;
    }

    public String getGioiTinh() {
        return gioiTinh;
    }

    public void setGioiTinh(String gioiTinh) {
        this.gioiTinh = gioiTinh;
    }

    public String getNgaySinh() {
        return ngaySinh;
    }

    public void setNgaySinh(String ngaySinh) {
        this.ngaySinh = ngaySinh;
    }

    public String getDiachi() {
        return diachi;
    }

    public void setDiachi(String diachi) {
        this.diachi = diachi;
    }

    public String getSoDienThoaiLienHe() {
        return soDienThoaiLienHe;
    }

    public void setSoDienThoaiLienHe(String soDienThoaiLienHe) {
        this.soDienThoaiLienHe = soDienThoaiLienHe;
    }

    @JsonIgnore
    public Phongban getPhongban() {
        return phongban;
    }

    public void setPhongban(Phongban phongban) {
        this.phongban = phongban;
    }

}
