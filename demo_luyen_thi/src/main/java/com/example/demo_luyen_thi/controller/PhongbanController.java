package com.example.demo_luyen_thi.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo_luyen_thi.model.Phongban;
import com.example.demo_luyen_thi.repository.NhanvienRepository;
import com.example.demo_luyen_thi.repository.PhongbanRepository;

@CrossOrigin
@RestController
public class PhongbanController {
    @Autowired
    PhongbanRepository phongbanRepository;

    @Autowired
    NhanvienRepository nhanvienRepository;

    @PostMapping("/phongban/create")
    public ResponseEntity<Object> createPhongban(@RequestBody Phongban phongban) {
        try {

            Phongban newRole = new Phongban();
            newRole.setMaPhongBan(phongban.getMaPhongBan());
            newRole.setTenPhongBan(phongban.getTenPhongBan());
            newRole.setNghiepVu(phongban.getNghiepVu());
            newRole.setGioiThieu(phongban.getGioiThieu());
            Phongban savedRole = phongbanRepository.save(newRole);

            return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Phòng ban: " + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/phongban/update/{id}")
    public ResponseEntity<Object> updatePhongban(@PathVariable("id") Long id, @RequestBody Phongban phongban) {
        Optional<Phongban> Data = phongbanRepository.findById(id);
        if (Data.isPresent()) {

            Phongban newData = Data.get();
            newData.setMaPhongBan(phongban.getMaPhongBan());
            newData.setTenPhongBan(phongban.getTenPhongBan());
            newData.setNghiepVu(phongban.getNghiepVu());
            newData.setGioiThieu(phongban.getGioiThieu());
            Phongban savedPhongban = phongbanRepository.save(newData);

            return new ResponseEntity<>(savedPhongban, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

     @DeleteMapping("/phongban/delete/{id}")
    public ResponseEntity<Object> deletePhongbanById(@PathVariable Long id) {
        try {
            Optional<Phongban> optional = phongbanRepository.findById(id);
            if (optional.isPresent()) {
                phongbanRepository.deleteById(id);
            } else {
                // countryRepository.deleteById(id);
            }
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/phongban/details/{id}")
    public Phongban getPhongbanById(@PathVariable Long id) {
        if (phongbanRepository.findById(id).isPresent())
            return phongbanRepository.findById(id).get();
        else
            return null;
    }

    @GetMapping("/phongban/all")
    public List<Phongban> getAllPhongbans() {
        return phongbanRepository.findAll();
    }
}
