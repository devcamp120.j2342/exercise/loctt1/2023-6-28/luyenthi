package com.example.demo_luyen_thi.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo_luyen_thi.model.Nhanvien;
import com.example.demo_luyen_thi.model.Phongban;
import com.example.demo_luyen_thi.repository.NhanvienRepository;
import com.example.demo_luyen_thi.repository.PhongbanRepository;

@CrossOrigin
@RestController
public class NhanvienController {
    @Autowired
    PhongbanRepository phongbanRepository;

    @Autowired
    NhanvienRepository nhanvienRepository;

    @PostMapping("/nhanvien/create/{id}")
    public ResponseEntity<Object> createNhanvien(@PathVariable("id") Long id, @RequestBody Nhanvien nhanvien) {
        try {
            Optional<Phongban> phongbanData = phongbanRepository.findById(id);
            if (phongbanData.isPresent()) {

                Nhanvien newRole = new Nhanvien();
                newRole.setMaNhanVien(nhanvien.getMaNhanVien());
                newRole.setTenNhanVien(nhanvien.getTenNhanVien());
                newRole.setChucVu(nhanvien.getChucVu());
                newRole.setGioiTinh(nhanvien.getGioiTinh());
                newRole.setNgaySinh(nhanvien.getNgaySinh());
                newRole.setDiachi(nhanvien.getDiachi());
                newRole.setSoDienThoaiLienHe(nhanvien.getSoDienThoaiLienHe());

                Phongban _nhanvien = phongbanData.get();
                newRole.setPhongban(_nhanvien);

                Nhanvien savedRole = nhanvienRepository.save(newRole);

                return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
            }
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Voucher: " + e.getCause().getCause().getMessage());
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PutMapping("/nhanvien/update/{id}")
    public ResponseEntity<Object> updateNhanvienr(@PathVariable("id") Long id, @RequestBody Nhanvien nhanvien) {
        Optional<Nhanvien> Data = nhanvienRepository.findById(id);
        if (Data.isPresent()) {

            Nhanvien newNhanvien = Data.get();
            newNhanvien.setMaNhanVien(nhanvien.getMaNhanVien());
            newNhanvien.setTenNhanVien(nhanvien.getTenNhanVien());
            newNhanvien.setChucVu(nhanvien.getChucVu());
            newNhanvien.setGioiTinh(nhanvien.getGioiTinh());
            newNhanvien.setNgaySinh(nhanvien.getNgaySinh());
            newNhanvien.setDiachi(nhanvien.getDiachi());
            newNhanvien.setSoDienThoaiLienHe(nhanvien.getSoDienThoaiLienHe());
            Nhanvien savedNhanvien = nhanvienRepository.save(newNhanvien);

            return new ResponseEntity<>(savedNhanvien, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/nhanvien/delete/{id}")
    public ResponseEntity<Object> deleteNhanvienById(@PathVariable Long id) {
        try {
            nhanvienRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/nhanvien/details/{id}")
    public Nhanvien getOrdersById(@PathVariable Long id) {
        if (nhanvienRepository.findById(id).isPresent())
            return nhanvienRepository.findById(id).get();
        else
            return null;
    }

    @GetMapping("/nhanvien/all")
    public List<Nhanvien> getAllOrder() {
        return nhanvienRepository.findAll();
    }

    @GetMapping("/phongban/{phongbanId}/nhanviens")
    public List<Nhanvien> getOrdersByUser(@PathVariable(value = "phongbanId") Long phongbanId) {
        return nhanvienRepository.findByPhongbanId(phongbanId);
    }

}
