package com.example.demo_luyen_thi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoLuyenThiApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoLuyenThiApplication.class, args);
	}

}
